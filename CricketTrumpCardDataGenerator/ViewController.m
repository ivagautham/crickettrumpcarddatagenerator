//
//  ViewController.m
//  CricketTrumpCardDataGenerator
//
//  Created by Gautham on 19/11/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "ViewController.h"
#import "TFHpple.h"
#import "FileManager.h"

#define BASE_URL @"http://www.espncricinfo.com"

#define TEST_CLASS 1
#define ODI_CLASS 2
#define T20_CLASS 3

#define PLAYER_NAME             @"Player Name"
#define PLAYER_ID               @"Player ID"
#define PLAYER_TEAM             @"Team"
#define PLAYER_CHOSEN           @"Player Chosen"
#define PLAYER_RELATIVE_PATH    @"Relative Path"

#define PLAYER_MATCHES          @"player_matches"
#define PLAYER_BAT_INNINGS      @"player_bat_innings"
#define PLAYER_NOT_OUTS         @"player_not_outs"
#define PLAYER_RUNS_SCORED      @"player_runs_scored"
#define PLAYER_BEST_BATTING     @"player_best_batting"
#define PLAYER_BAT_AVERAGE      @"player_bat_average"
#define PLAYER_BALLS_FACED      @"player_balls_faces"
#define PLAYER_BAT_STRIKE_RATE  @"player_bat_strike_rate"
#define PLAYER_HUNDREDS         @"player_hundreds"
#define PLAYER_FIFTIES          @"player_fifties"
#define PLAYER_FOURS            @"player_fours"
#define PLAYER_SIXES            @"player_sixes"
#define PLAYER_CATCHES          @"player_catches"
#define PLAYER_STUMPINGS        @"player_stumpings"

#define PLAYER_BOWL_INNINGS     @"player_bowl_innings"
#define PLAYER_BALLS_BOWLED     @"player_balls_bowled"
#define PLAYER_RUNS_CONCEDED    @"player_runs_conceded"
#define PLAYER_WICKETS          @"player_wickets"
#define PLAYER_BEST_BOWLING_INNINGS @"player_best_bowling_innings"
#define PLAYER_BEST_BOWLING_MATCH   @"player_best_bowling_match"
#define PLAYER_BOWL_AVERAGE     @"player_bowl_average"
#define PLAYER_ECON             @"player_econ"
#define PLAYER_BOWL_STRIKE_RATE @"player_bowl_strike_rate"
#define PLAYER_FOUR_FORS        @"player_four_fors"
#define PLAYER_FIVE_FORS        @"player_five_fors"
#define PLAYER_TEN_FORS         @"player_ten_fors"

#define PLAYER_GRADE            @"player_grade"
#define PLAYER_PRICE            @"player_price"

@interface ViewController ()
{
    FileManager *FM;
    NSMutableArray *Teams;
    NSMutableArray *allChosenPlayers;
    NSMutableArray *completePlayerDetailsList;
    NSNumber *idx;
    int indexValue;
    NSMutableDictionary *toAppendDictionary;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    FM = [[FileManager alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    idx = [defaults objectForKey:@"player_index"];
    if(idx)
        indexValue = [idx intValue];
    else
        indexValue = 0;
    
    [self onLoadPlayer];
    
    toAppendDictionary = [[NSMutableDictionary alloc] init];
    [toAppendDictionary removeAllObjects];
    //[self prepareTofetchAllPlayers];
    //[self prepareTofetchDetailsOfPlayers];
    
    
    //    [self updatePlayerStats];
    //    NSData *JSONData = [NSJSONSerialization dataWithJSONObject:completePlayerDetailsList options:kNilOptions error:nil];
    //    [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"allPlayerDetails.json"]];
    
    //[self openAndsave];
}

-(void)openAndsave
{
    NSMutableArray *mainArray = [[NSMutableArray alloc] init];
    [mainArray removeAllObjects];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"allChosenPlayers" ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [allChosenPlayers removeAllObjects];
    NSError *jsonError;
    allChosenPlayers = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    NSLog(@"Started");
    
    completePlayerDetailsList = [[NSMutableArray alloc] init];
    [completePlayerDetailsList removeAllObjects];
    for(NSDictionary *playerDict in allChosenPlayers)
    {
        NSNumber *playerID = [playerDict valueForKey:PLAYER_ID];
        
        NSString* path = [[NSBundle mainBundle] pathForResource:[playerID stringValue] ofType:@"json"];
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
        NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableDictionary *newPlayer = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
        
        [mainArray addObject:newPlayer];
    }
    NSData * JSONData = [NSJSONSerialization dataWithJSONObject:mainArray options:kNilOptions error:nil];
    [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"allPlayerDetails.json"]];
    
    
}

-(void)prepareTofetchDetailsOfPlayers
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"allChosenPlayers" ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [allChosenPlayers removeAllObjects];
    NSError *jsonError;
    allChosenPlayers = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    NSLog(@"Started");
    
    completePlayerDetailsList = [[NSMutableArray alloc] init];
    [completePlayerDetailsList removeAllObjects];
    for(NSDictionary *playerDict in allChosenPlayers)
    {
        //[self fetchImageForplayer:playerDict];
        [self fetchDetailsForplayer:playerDict];
    }
    NSData * JSONData = [NSJSONSerialization dataWithJSONObject:completePlayerDetailsList options:kNilOptions error:nil];
    [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"allPlayerDetails.json"]];
    
    NSLog(@"Ended");
    
    //[self fetchDetailsForplayer:[allChosenPlayers objectAtIndex:0]];
}

-(void)fetchImageForplayer:(NSDictionary *)player
{
    NSString *playerID = [player valueForKey:PLAYER_ID];
    
    NSString *playerRelativeURL = [player valueForKey:PLAYER_RELATIVE_PATH];
    NSString *playerString = [NSString stringWithFormat:@"%@/%@",BASE_URL,playerRelativeURL];
    NSURL *playerUrl = [NSURL URLWithString:playerString];
    NSData *playerHtmlData = [NSData dataWithContentsOfURL:playerUrl];
    
    TFHpple *playerParser = [TFHpple hppleWithHTMLData:playerHtmlData];
    NSString *playerImageQueryString = @"//link[@rel='image_src']";
    TFHppleElement *playerImageElement = [playerParser peekAtSearchWithXPathQuery:playerImageQueryString];
    NSString *playerImageString = [playerImageElement.attributes valueForKey:@"href"];
    NSURL *playerImageUrl = [NSURL URLWithString:playerImageString];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",playerID]];
    NSData *thedata = [NSData dataWithContentsOfURL:playerImageUrl];
    [thedata writeToFile:localFilePath atomically:YES];
}

-(void)fetchDetailsForplayer:(NSDictionary *)player
{
    NSMutableDictionary *completePlayerDetails = [[NSMutableDictionary alloc] init];
    NSString *playerName = [[player valueForKey:PLAYER_NAME] capitalizedString];
    [completePlayerDetails setValue:playerName forKey:PLAYER_NAME];
    NSString *playerID = [player valueForKey:PLAYER_ID];
    [completePlayerDetails setValue:playerID forKey:PLAYER_ID];
    NSString *playerTeam = [[player valueForKey:PLAYER_TEAM] capitalizedString];
    [completePlayerDetails setValue:playerTeam forKey:PLAYER_TEAM];
    
    NSString *playerRelativeURL = [player valueForKey:PLAYER_RELATIVE_PATH];
    NSString *playerString = [NSString stringWithFormat:@"%@/%@",BASE_URL,playerRelativeURL];
    NSURL *playerUrl = [NSURL URLWithString:playerString];
    NSData *playerHtmlData = [NSData dataWithContentsOfURL:playerUrl];
    
    TFHpple *playerParser = [TFHpple hppleWithHTMLData:playerHtmlData];
    
    NSString *playerImageQueryString = @"//tr[@class='data1']";
    // NSString *playerQueryString = @"//p[@class='ciPlayerinformationtxt']";
    NSArray *playerImageNodes = [playerParser searchWithXPathQuery:playerImageQueryString];
    BOOL battingData = TRUE;
    for (TFHppleElement *elemet in playerImageNodes)
    {
        //Check if only ODIs
        TFHppleElement *firstEle = [elemet firstChildWithTagName:@"td"];
        TFHppleElement *grandChild = firstEle.firstChild;
        NSString *format = grandChild.firstChild.content;
        if ([format isEqualToString:@"ODIs"])
        {
            if (battingData)
            {
                NSMutableArray *requiredArrayElement =[NSMutableArray arrayWithArray:[elemet childrenWithTagName:@"td"]];
                
                if ([requiredArrayElement count] > 0)
                    [requiredArrayElement removeObjectAtIndex:0];
                
                for (int index = 0; index < [requiredArrayElement count]; index++)
                {
                    TFHppleElement *requiredElements = [requiredArrayElement objectAtIndex:index];
                    
                    NSString *values = requiredElements.firstChild.content;
                    switch (index)
                    {
                        case 0:
                            [completePlayerDetails setValue:values forKey:PLAYER_MATCHES];
                            break;
                        case 1:
                            [completePlayerDetails setValue:values forKey:PLAYER_BAT_INNINGS];
                            break;
                        case 2:
                            [completePlayerDetails setValue:values forKey:PLAYER_NOT_OUTS];
                            break;
                        case 3:
                            [completePlayerDetails setValue:values forKey:PLAYER_RUNS_SCORED];
                            break;
                        case 4:
                            [completePlayerDetails setValue:values forKey:PLAYER_BEST_BATTING];
                            break;
                        case 5:
                            [completePlayerDetails setValue:values forKey:PLAYER_BAT_AVERAGE];
                            break;
                        case 6:
                            [completePlayerDetails setValue:values forKey:PLAYER_BALLS_FACED];
                            break;
                        case 7:
                            [completePlayerDetails setValue:values forKey:PLAYER_BAT_STRIKE_RATE];
                            break;
                        case 8:
                            [completePlayerDetails setValue:values forKey:PLAYER_HUNDREDS];
                            break;
                        case 9:
                            [completePlayerDetails setValue:values forKey:PLAYER_FIFTIES];
                            break;
                        case 10:
                            [completePlayerDetails setValue:values forKey:PLAYER_FOURS];
                            break;
                        case 11:
                            [completePlayerDetails setValue:values forKey:PLAYER_SIXES];
                            break;
                        case 12:
                            [completePlayerDetails setValue:values forKey:PLAYER_CATCHES];
                            break;
                        case 13:
                            [completePlayerDetails setValue:values forKey:PLAYER_STUMPINGS];
                            break;
                        default:
                            break;
                    }
                }
                battingData = FALSE;
            }
            else
            {
                NSMutableArray *requiredArrayElement =[NSMutableArray arrayWithArray:[elemet childrenWithTagName:@"td"]];
                
                if ([requiredArrayElement count] > 0)
                    [requiredArrayElement removeObjectAtIndex:0];
                
                for (int index = 0; index < [requiredArrayElement count]; index++)
                {
                    TFHppleElement *requiredElements = [requiredArrayElement objectAtIndex:index];
                    NSString *values = requiredElements.firstChild.content;
                    switch (index)
                    {
                        case 1:
                            [completePlayerDetails setValue:values forKey:PLAYER_BOWL_INNINGS];
                            break;
                        case 2:
                            [completePlayerDetails setValue:values forKey:PLAYER_BALLS_BOWLED];
                            break;
                        case 3:
                            [completePlayerDetails setValue:values forKey:PLAYER_RUNS_CONCEDED];
                            break;
                        case 4:
                            [completePlayerDetails setValue:values forKey:PLAYER_WICKETS];
                            break;
                        case 5:
                            [completePlayerDetails setValue:values forKey:PLAYER_BEST_BOWLING_INNINGS];
                            break;
                        case 6:
                            [completePlayerDetails setValue:values forKey:PLAYER_BEST_BOWLING_MATCH];
                            break;
                        case 7:
                            [completePlayerDetails setValue:values forKey:PLAYER_BOWL_AVERAGE];
                            break;
                        case 8:
                            [completePlayerDetails setValue:values forKey:PLAYER_ECON];
                            break;
                        case 9:
                            [completePlayerDetails setValue:values forKey:PLAYER_BOWL_STRIKE_RATE];
                            break;
                        case 10:
                            [completePlayerDetails setValue:values forKey:PLAYER_FOUR_FORS];
                            break;
                        case 11:
                            [completePlayerDetails setValue:values forKey:PLAYER_FIVE_FORS];
                            break;
                        case 12:
                            [completePlayerDetails setValue:values forKey:PLAYER_TEN_FORS];
                            break;
                        default:
                            break;
                    }
                }
                //save and return
                NSLog(@"player Index: %@",completePlayerDetails);
                [completePlayerDetailsList addObject:completePlayerDetails];
                NSData * JSONData = [NSJSONSerialization dataWithJSONObject:completePlayerDetails options:kNilOptions error:nil];
                [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"%@.json",playerID]];
                return;
            }
        }
    }
}

-(void)prepareTofetchAllPlayers
{
    NSString *TeamsPlistPath = [[NSString alloc] init];
    TeamsPlistPath = [[NSBundle mainBundle] pathForResource:
                      @"Teams" ofType:@"plist"];
    Teams = [[NSMutableArray alloc] initWithContentsOfFile:TeamsPlistPath];
    for (NSDictionary *team in Teams)
    {
        NSString *mTeam = [team valueForKey:PLAYER_TEAM];
        int mTeamID = [[team valueForKey:@"TeamID"] intValue];
        [self fetchAllPlayersforTeam:mTeam andTeamID:mTeamID];
    }
}

-(void)fetchAllPlayersforTeam:(NSString *)team andTeamID:(int)teamID
{
    NSString *teamString = [NSString stringWithFormat:@"%@/%@/content/player/caps.html?country=%d;class=%d",BASE_URL, team, teamID, ODI_CLASS];
    NSURL *teamURL = [NSURL URLWithString:teamString];
    NSData *teamHtmlData = [NSData dataWithContentsOfURL:teamURL];
    
    TFHpple *teamParser = [TFHpple hppleWithHTMLData:teamHtmlData];
    
    NSString *teamQueryString = @"//a";
    NSArray *teamNodes = [teamParser searchWithXPathQuery:teamQueryString];
    NSString *toSearch =[NSString stringWithFormat:@"/%@/content/player/",team];
    NSMutableArray *teamPlayers = [[NSMutableArray alloc] init];
    for (TFHppleElement *elemet in teamNodes)
    {
        if ([[elemet.raw uppercaseString] rangeOfString:[toSearch uppercaseString]].location != NSNotFound)
        {
            NSString *playerName = elemet.firstChild.content;
            NSString *relativePathString = [elemet.attributes valueForKey:@"href"];
            NSURL *relativePath = [NSURL URLWithString:relativePathString];
            NSArray *item = [[relativePath lastPathComponent] componentsSeparatedByString:@"."];
            NSString *playerID = [item objectAtIndex:0];
            
            NSLog(@"\n Player : %@ \n ID : %@ \n Path : %@",playerName, playerID, relativePath);
            NSDictionary *playerDetails = [[NSDictionary alloc] initWithObjectsAndKeys:playerID,PLAYER_ID, playerName,PLAYER_NAME, team, PLAYER_TEAM, relativePathString, PLAYER_RELATIVE_PATH, @"NO", PLAYER_CHOSEN, nil];
            [teamPlayers addObject:playerDetails];
        }
    }
    
    NSMutableArray *unique = [NSMutableArray array];
    
    for (id obj in teamPlayers) {
        if (![unique containsObject:obj]) {
            [unique addObject:obj];
        }
    }
    [teamPlayers removeAllObjects];
    [teamPlayers addObjectsFromArray:unique];
    
    NSData * JSONData = [NSJSONSerialization dataWithJSONObject:teamPlayers options:kNilOptions error:nil];
    [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"%@.json",team]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onLoadPlayer
{
    [toAppendDictionary removeAllObjects];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"allPlayerDetails" ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [completePlayerDetailsList removeAllObjects];
    NSError *jsonError;
    completePlayerDetailsList = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    
    NSDictionary *currentPlayer = [completePlayerDetailsList objectAtIndex:indexValue];
    
    NSString *imageName = [ NSString stringWithFormat:@"%@.jpg",[currentPlayer valueForKey:PLAYER_ID]];
    _image.image = [UIImage imageNamed:imageName];
    _index.text = [NSString stringWithFormat:@"%d",indexValue+1];
    
    _pid.text = [[currentPlayer valueForKey:PLAYER_ID] stringValue];
    _name.text = [currentPlayer valueForKey:PLAYER_NAME];
    _team.text = [currentPlayer valueForKey:PLAYER_TEAM];
    
    _matches.text = [currentPlayer valueForKey:PLAYER_MATCHES];
    _catches.text = [currentPlayer valueForKey:PLAYER_CATCHES];
    _stumping.text = [currentPlayer valueForKey:PLAYER_STUMPINGS];
    
    _bat_runs.text = [currentPlayer valueForKey:PLAYER_RUNS_SCORED];
    _bat_NO.text = [currentPlayer valueForKey:PLAYER_NOT_OUTS];
    _bat_avg.text = [currentPlayer valueForKey:PLAYER_BAT_AVERAGE];
    _bat_best.text = [currentPlayer valueForKey:PLAYER_BEST_BATTING];
    _bat_SR.text = [currentPlayer valueForKey:PLAYER_BAT_STRIKE_RATE];
    _bat_100s.text = [currentPlayer valueForKey:PLAYER_HUNDREDS];
    _bat_50s.text = [currentPlayer valueForKey:PLAYER_FIFTIES];
    
    _bowl_wickets.text = [currentPlayer valueForKey:PLAYER_WICKETS];
    _bowl_balls.text = [currentPlayer valueForKey:PLAYER_BALLS_BOWLED];
    _bowl_average.text = [currentPlayer valueForKey:PLAYER_BOWL_AVERAGE];
    _bowl_best.text = [currentPlayer valueForKey:PLAYER_BEST_BOWLING_MATCH];
    _bowl_sr.text = [currentPlayer valueForKey:PLAYER_BOWL_STRIKE_RATE];
    _bowl_econ.text = [currentPlayer valueForKey:PLAYER_ECON];
    _bowl_5for.text = [currentPlayer valueForKey:PLAYER_FIVE_FORS];
}

- (IBAction)onNext:(id)sender
{
    if (indexValue == completePlayerDetailsList.count)
    {
        [self updatePlayerDetails];
        [self onLoadPlayer];
        return;
    }
    [self updatePlayerDetails];
    indexValue++;
    idx = [NSNumber numberWithInt:indexValue];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idx forKey:@"player_index"];
    [defaults synchronize];
    [self onLoadPlayer];
}

- (IBAction)onPrev:(id)sender
{
    if (indexValue == 0)
    {
        [self updatePlayerDetails];
        [self onLoadPlayer];
        return;
    }
    
    
    [self updatePlayerDetails];
    
    indexValue--;
    idx = [NSNumber numberWithInt:indexValue];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idx forKey:@"player_index"];
    [defaults synchronize];
    [self onLoadPlayer];
}

-(void)updatePlayerStats
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"allPlayerDetails" ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [completePlayerDetailsList removeAllObjects];
    NSError *jsonError;
    completePlayerDetailsList = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    
    NSMutableDictionary *completePlayerDetails = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *tempCPDL = [completePlayerDetailsList mutableCopy];
    for (NSDictionary *dict in tempCPDL)
    {
        NSMutableDictionary *currentPlayer = [NSMutableDictionary dictionaryWithDictionary:dict];
        NSNumber *playerID = [currentPlayer valueForKey:PLAYER_ID];
        NSString *playerRelativeURL = [self urlForPlayerID:playerID];
        NSString *playerString = [NSString stringWithFormat:@"%@/%@",BASE_URL,playerRelativeURL];
        NSURL *playerUrl = [NSURL URLWithString:playerString];
        NSData *playerHtmlData = [NSData dataWithContentsOfURL:playerUrl];
        
        TFHpple *playerParser = [TFHpple hppleWithHTMLData:playerHtmlData];
        
        NSString *playerImageQueryString = @"//tr[@class='data1']";
        // NSString *playerQueryString = @"//p[@class='ciPlayerinformationtxt']";
        NSArray *playerImageNodes = [playerParser searchWithXPathQuery:playerImageQueryString];
        BOOL battingData = TRUE;
        for (TFHppleElement *elemet in playerImageNodes)
        {
            //Check if only ODIs
            TFHppleElement *firstEle = [elemet firstChildWithTagName:@"td"];
            TFHppleElement *grandChild = firstEle.firstChild;
            NSString *format = grandChild.firstChild.content;
            if ([format isEqualToString:@"ODIs"])
            {
                if (battingData)
                {
                    NSMutableArray *requiredArrayElement =[NSMutableArray arrayWithArray:[elemet childrenWithTagName:@"td"]];
                    
                    if ([requiredArrayElement count] > 0)
                        [requiredArrayElement removeObjectAtIndex:0];
                    
                    for (int index = 0; index < [requiredArrayElement count]; index++)
                    {
                        TFHppleElement *requiredElements = [requiredArrayElement objectAtIndex:index];
                        
                        NSString *values = requiredElements.firstChild.content;
                        switch (index)
                        {
                            case 0:
                                [completePlayerDetails setValue:values forKey:PLAYER_MATCHES];
                                [currentPlayer removeObjectForKey:PLAYER_MATCHES];
                                break;
                            case 1:
                                [completePlayerDetails setValue:values forKey:PLAYER_BAT_INNINGS];
                                [currentPlayer removeObjectForKey:PLAYER_BAT_INNINGS];
                                break;
                            case 2:
                                [completePlayerDetails setValue:values forKey:PLAYER_NOT_OUTS];
                                [currentPlayer removeObjectForKey:PLAYER_NOT_OUTS];
                                break;
                            case 3:
                                [completePlayerDetails setValue:values forKey:PLAYER_RUNS_SCORED];
                                [currentPlayer removeObjectForKey:PLAYER_RUNS_SCORED];
                                break;
                            case 4:
                                [completePlayerDetails setValue:values forKey:PLAYER_BEST_BATTING];
                                [currentPlayer removeObjectForKey:PLAYER_BEST_BATTING];
                                break;
                            case 5:
                                [completePlayerDetails setValue:values forKey:PLAYER_BAT_AVERAGE];
                                [currentPlayer removeObjectForKey:PLAYER_BAT_AVERAGE];
                                break;
                            case 6:
                                [completePlayerDetails setValue:values forKey:PLAYER_BALLS_FACED];
                                [currentPlayer removeObjectForKey:PLAYER_BALLS_FACED];
                                break;
                            case 7:
                                [completePlayerDetails setValue:values forKey:PLAYER_BAT_STRIKE_RATE];
                                [currentPlayer removeObjectForKey:PLAYER_BAT_STRIKE_RATE];
                                break;
                            case 8:
                                [completePlayerDetails setValue:values forKey:PLAYER_HUNDREDS];
                                [currentPlayer removeObjectForKey:PLAYER_HUNDREDS];
                                break;
                            case 9:
                                [completePlayerDetails setValue:values forKey:PLAYER_FIFTIES];
                                [currentPlayer removeObjectForKey:PLAYER_FIFTIES];
                                break;
                            case 10:
                                [completePlayerDetails setValue:values forKey:PLAYER_FOURS];
                                [currentPlayer removeObjectForKey:PLAYER_FOURS];
                                break;
                            case 11:
                                [completePlayerDetails setValue:values forKey:PLAYER_SIXES];
                                [currentPlayer removeObjectForKey:PLAYER_SIXES];
                                break;
                            case 12:
                                [completePlayerDetails setValue:values forKey:PLAYER_CATCHES];
                                [currentPlayer removeObjectForKey:PLAYER_CATCHES];
                                break;
                            case 13:
                                [completePlayerDetails setValue:values forKey:PLAYER_STUMPINGS];
                                [currentPlayer removeObjectForKey:PLAYER_STUMPINGS];
                                break;
                            default:
                                break;
                        }
                    }
                    battingData = FALSE;
                }
                else
                {
                    NSMutableArray *requiredArrayElement =[NSMutableArray arrayWithArray:[elemet childrenWithTagName:@"td"]];
                    
                    if ([requiredArrayElement count] > 0)
                        [requiredArrayElement removeObjectAtIndex:0];
                    
                    for (int index = 0; index < [requiredArrayElement count]; index++)
                    {
                        TFHppleElement *requiredElements = [requiredArrayElement objectAtIndex:index];
                        NSString *values = requiredElements.firstChild.content;
                        switch (index)
                        {
                            case 1:
                                [completePlayerDetails setValue:values forKey:PLAYER_BOWL_INNINGS];
                                [currentPlayer removeObjectForKey:PLAYER_BOWL_INNINGS];
                                break;
                            case 2:
                                [completePlayerDetails setValue:values forKey:PLAYER_BALLS_BOWLED];
                                [currentPlayer removeObjectForKey:PLAYER_BALLS_BOWLED];
                                break;
                            case 3:
                                [completePlayerDetails setValue:values forKey:PLAYER_RUNS_CONCEDED];
                                [currentPlayer removeObjectForKey:PLAYER_RUNS_CONCEDED];
                                break;
                            case 4:
                                [completePlayerDetails setValue:values forKey:PLAYER_WICKETS];
                                [currentPlayer removeObjectForKey:PLAYER_WICKETS];
                                break;
                            case 5:
                                [completePlayerDetails setValue:values forKey:PLAYER_BEST_BOWLING_INNINGS];
                                [currentPlayer removeObjectForKey:PLAYER_BEST_BOWLING_INNINGS];
                                break;
                            case 6:
                                [completePlayerDetails setValue:values forKey:PLAYER_BEST_BOWLING_MATCH];
                                [currentPlayer removeObjectForKey:PLAYER_BEST_BOWLING_MATCH];
                                break;
                            case 7:
                                [completePlayerDetails setValue:values forKey:PLAYER_BOWL_AVERAGE];
                                [currentPlayer removeObjectForKey:PLAYER_BOWL_AVERAGE];
                                break;
                            case 8:
                                [completePlayerDetails setValue:values forKey:PLAYER_ECON];
                                [currentPlayer removeObjectForKey:PLAYER_ECON];
                                break;
                            case 9:
                                [completePlayerDetails setValue:values forKey:PLAYER_BOWL_STRIKE_RATE];
                                [currentPlayer removeObjectForKey:PLAYER_BOWL_STRIKE_RATE];
                                break;
                            case 10:
                                [completePlayerDetails setValue:values forKey:PLAYER_FOUR_FORS];
                                [currentPlayer removeObjectForKey:PLAYER_FOUR_FORS];
                                break;
                            case 11:
                                [completePlayerDetails setValue:values forKey:PLAYER_FIVE_FORS];
                                [currentPlayer removeObjectForKey:PLAYER_FIVE_FORS];
                                break;
                            case 12:
                                [completePlayerDetails setValue:values forKey:PLAYER_TEN_FORS];
                                [currentPlayer removeObjectForKey:PLAYER_TEN_FORS];
                                break;
                            default:
                                break;
                        }
                    }
                    //save and return
                    [completePlayerDetailsList removeObject:dict];
                    [currentPlayer addEntriesFromDictionary:completePlayerDetails];
                    NSLog(@"player Index: %@",currentPlayer);
                    [completePlayerDetailsList addObject:completePlayerDetails];
                    NSData * JSONData = [NSJSONSerialization dataWithJSONObject:completePlayerDetailsList options:kNilOptions error:nil];
                    [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"%@.json",playerID]];
                }
            }
        }
        
    }
    
}

-(NSString *)urlForPlayerID:(NSNumber *)pid
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"allChosenPlayers" ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [allChosenPlayers removeAllObjects];
    NSError *jsonError;
    allChosenPlayers = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    NSLog(@"Started");
    for(NSDictionary *playerDict in allChosenPlayers)
    {
        NSNumber *playerID = [playerDict valueForKey:PLAYER_ID];
        if ([pid isEqualToNumber:playerID])
        {
            NSString *playerRelativeURL = [playerDict valueForKey:PLAYER_RELATIVE_PATH];
            return playerRelativeURL;
        }
    }
    return nil;
}
-(void)updatePlayerDetails
{
    NSMutableDictionary *currentPlayer = [NSMutableDictionary dictionaryWithDictionary:[completePlayerDetailsList objectAtIndex:indexValue]];
    [completePlayerDetailsList removeObject:currentPlayer];
    NSString *playerGrade = [_grade titleForSegmentAtIndex:_grade.selectedSegmentIndex];
    NSString *playerPrice = [_price titleForSegmentAtIndex:_price.selectedSegmentIndex];
    
    [toAppendDictionary setValue:playerGrade forKey:PLAYER_GRADE];
    [toAppendDictionary setValue:playerPrice forKey:PLAYER_PRICE];
    [currentPlayer addEntriesFromDictionary:toAppendDictionary];
    
    NSString *playerID = [currentPlayer valueForKey:PLAYER_ID];
    [completePlayerDetailsList addObject:currentPlayer];
    
    NSData * JSONData = [NSJSONSerialization dataWithJSONObject:currentPlayer options:kNilOptions error:nil];
    [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"%@.json",playerID]];
    
    JSONData = [NSJSONSerialization dataWithJSONObject:completePlayerDetailsList options:kNilOptions error:nil];
    [FM writeContents:JSONData ToTextFile:[NSString stringWithFormat:@"allPlayerDetails.json"]];
}
@end
