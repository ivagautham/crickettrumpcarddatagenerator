//
//  FileManager.h
//  MTCChennaiInfo
//
//  Created by Gautham on 12/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
{
    
}

-(BOOL) writeContents:(NSData *)contents ToTextFile:(NSString *)fileName;
-(NSString *) displayContentsOfFile:(NSString *)fileName;

@end
