//
//  FileManager.m
//  MTCChennaiInfo
//
//  Created by Gautham on 12/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

-(BOOL) writeContents:(NSData *)contents ToTextFile:(NSString *)fileName
{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    fileName = [NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName];
    
    //Truncate file before writing
    NSFileHandle *file;
    file = [NSFileHandle fileHandleForUpdatingAtPath: fileName];
    if (!file)
        NSLog(@"Failed to open file");
    [file truncateFileAtOffset: 0];
    [file closeFile];
    
    //save content to the documents directory
    return [contents writeToFile:fileName options:NSDataWritingAtomic error:nil];
}


//Method retrieves content from documents directory and
//displays it in an alert
-(NSString *) displayContentsOfFile:(NSString *)fileName
{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    fileName = [NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName];
    
    NSString *content = [[NSString alloc] initWithContentsOfFile:fileName usedEncoding:nil error:nil];
    return content;
}

@end
