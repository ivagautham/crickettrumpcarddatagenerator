//
//  ViewController.h
//  CricketTrumpCardDataGenerator
//
//  Created by Gautham on 19/11/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *index;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UILabel *pid;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *team;

@property (weak, nonatomic) IBOutlet UILabel *matches;
@property (weak, nonatomic) IBOutlet UILabel *catches;
@property (weak, nonatomic) IBOutlet UILabel *stumping;

@property (weak, nonatomic) IBOutlet UILabel *bat_runs;
@property (weak, nonatomic) IBOutlet UILabel *bat_NO;
@property (weak, nonatomic) IBOutlet UILabel *bat_avg;
@property (weak, nonatomic) IBOutlet UILabel *bat_best;
@property (weak, nonatomic) IBOutlet UILabel *bat_SR;
@property (weak, nonatomic) IBOutlet UILabel *bat_100s;
@property (weak, nonatomic) IBOutlet UILabel *bat_50s;

@property (weak, nonatomic) IBOutlet UILabel *bowl_wickets;
@property (weak, nonatomic) IBOutlet UILabel *bowl_balls;
@property (weak, nonatomic) IBOutlet UILabel *bowl_average;
@property (weak, nonatomic) IBOutlet UILabel *bowl_best;
@property (weak, nonatomic) IBOutlet UILabel *bowl_sr;
@property (weak, nonatomic) IBOutlet UILabel *bowl_econ;
@property (weak, nonatomic) IBOutlet UILabel *bowl_5for;

@property (weak, nonatomic) IBOutlet UISegmentedControl *grade;
@property (weak, nonatomic) IBOutlet UISegmentedControl *price;

- (IBAction)onNext:(id)sender;
- (IBAction)onPrev:(id)sender;

@end
