//
//  AppDelegate.h
//  CricketTrumpCardDataGenerator
//
//  Created by Gautham on 19/11/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
